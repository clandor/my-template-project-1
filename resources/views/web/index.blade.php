@extends('web.app')

@section('content')
    <div class="pad"></div>
    <div class="content blog">

        <div class="blogpostcategory">
            <div class="topBlog">

                <h2 class="title"><a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">The Story of Arthas Menethil</a></h2>
                <div class="post-meta">
                    <a class="post-meta-time" href="2016/11/22/index.html">November 22, 2016</a> <a class="post-meta-author" href="index.html">by Freyja</a> <a href="index.html@p=4752#commentform">No Comments</a>
                </div>
                <!-- end of post meta -->
            </div>


            <a class="overdefultlink" href="index.html@p=4752">
                <div class="overdefult"></div>
            </a>

            <div class="blogimage">
                <div class="loading"></div>
                <a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil"><img width="1180" height="770" src="{{asset('assets/img/freyja-image-16-1180x770.jpg')}}" class="attachment-freya-postBlock size-freya-postBlock wp-post-image" alt="freyja-image-16" /></a>
            </div>

            <div class="entry">
                <div class="meta">
                    <div class="blogContent">
                        <div class="blogcontent"><span class="su-dropcap su-dropcap-style-simple" style="font-size:2.5em">A</span>
                            <p><b>rthas Menethil</b>, Crown Prince of <a title="Lordaeron" href="http://wowwiki.wikia.com/wiki/Lordaeron">Lordaeron</a> and Knight of the Silver Hand, was the son of <strong>King Terenas Menethil II</strong> and heir to the throne. He was trained as a paladin by <a class="mw-redirect" title="Uther Lightbringer" href="http://wowwiki.wikia.com/wiki/Uther_Lightbringer">Uther the Lightbringer</a>, and had a romantic relationship with the kind sorceress <a title="Jaina Proudmoore" href="http://wowwiki.wikia.com/wiki/Jaina_Proudmoore">Jaina Proudmoore</a>. </p>
                            <p>Despite his promising beginnings, Arthas became one of the most powerful and evil beings <a title="Azeroth (world)" href="http://wowwiki.wikia.com/wiki/Azeroth_(world)">Azeroth</a> would ever know. Taking up the cursed runeblade <a title="Frostmourne" href="http://wowwiki.wikia.com/wiki/Frostmourne">Frostmourne</a>, he became a death knight, led the Scourge in destroying Lordaeron, and merged with the <a class="mw-redirect" title="Lich King" href="http://wowwiki.wikia.com/wiki/Lich_King">Lich King</a>. Ruling as the dominant personality of the Lich
                                King for years afterwards, Arthas was defeated in combat by <a title="Adventurer" href="http://wowwiki.wikia.com/wiki/Adventurer">adventurers</a> of the Alliance and the Horde. Cradled by the spirit of his father, King Terenas, Arthas Menethil died, leaving the mantle of the <strong>Lich King</strong> to be taken by a noble soul who would contain the power of the Scourge.</p>
                            <div class="su-row">
                                <div class="su-column su-column-size-1-2">
                                    <div class="su-column-inner su-clearfix">This is the <strong>1st Column</strong>. Prince Arthas Menethil was born by Lianne Menethil to <em>King Terenas Menethil II</em>, four years before the start of the First War and is their youngest child. The young prince grew up in a time when the lands of Azeroth were ravaged.<br />
                                        <img class="alignnone wp-image-6601" src="{{asset('assets/img/arthas_menethil-wallpaper-1280x800.jpg')}}" alt="arthas_menethil-wallpaper-1280x800" width="380" height="233" srcset="{{asset('assets/img/arthas_menethil-wallpaper-1280x800.jpg')}} 800w, {{asset('assets/img/arthas_menethil-wallpaper-1280x800-300x184.jpg')}} 300w, {{asset('assets/img/arthas_menethil-wallpaper-1280x800-768x470.jpg')}} 768w, {{asset('assets/img/arthas_menethil-wallpaper-1280x800-600x368.jpg')}} 600w" sizes="(max-width: 380px) 100vw, 380px" /></div>
                                </div>
                                <div class="su-column su-column-size-1-2">
                                    <div class="su-column-inner su-clearfix"> This is the <strong>2nd Column</strong>. As a youth, Arthas was trained in combat by Muradin Bronzebeard, the brother of the dwarven king Magni Bronzebeard, and became an adept swordsman. Under the tutelage of Uther the Lightbringer, Arthas was inducted into the Knights of the Silver Hand at the young age of 19. The ceremony was held in the <a title="Cathedral of Light" href="http://wowwiki.wikia.com/wiki/Cathedral_of_Light">Cathedral of Light</a> in
                                        <a title="Stormwind City" href="http://wowwiki.wikia.com/wiki/Stormwind_City">Stormwind City</a>, and it was then that Arthas was given the holy mace called Light&#8217;s Vengeance. Despite his rash and headstrong behavior, Arthas became a renowned warrior. One of his more famed exploits was counterattacking a group of forest trolls striking at <a title="Quel'Thalas" href="http://wowwiki.wikia.com/wiki/Quel%27Thalas">Quel&#8217;Thalas</a> from <a title="Zul'Aman" href="http://wowwiki.wikia.com/wiki/Zul%27Aman">Zul&#8217;Aman</a>.
                                    </div>
                                </div>
                            </div>
                            <p>It was during this time that Arthas met the youngest daughter of Daelin Proudmoore, the sorceress Jaina. Over the years, they grew close as friends, and then romantically. They were very much in love with one another. But, eventually Arthas would question whether the two of them were ready to be together. Arthas would abruptly end the relationship so Jaina could focus on her magical studies in <a title="Dalaran" href="http://wowwiki.wikia.com/wiki/Dalaran">Dalaran</a> and Arthas could focus on his commitments to
                                <a title="Lordaeron" href="http://wowwiki.wikia.com/wiki/Lordaeron">Lordaeron</a>. Shortly after, they would agree to rekindle their romance, but this was during the beginning of the Scourge invasion that would change both of their lives forever.</p>
                            <div class="su-quote su-quote-style-default su-quote-has-cite">
                                <div class="su-quote-inner su-clearfix"><span class="su-quote-cite">This is the best restaurant I have ever visited. Great location and great food. The only minor downside are the prices which are pretty high but I guess quality comes with the price. - <strong>John Barbista</strong></span></div>
                            </div>
                            <p>Troubles began to stir in Lordaeron. <a class="mw-redirect" title="Orcs" href="http://wowwiki.wikia.com/wiki/Orcs">Orcs</a> broke free of their internment camps, and there was distressing news of a plague that had gripped the northlands. Arthas and Uther were sent to <a title="Strahnbrad" href="http://wowwiki.wikia.com/wiki/Strahnbrad">Strahnbrad</a> to defend the town from orcish raids. The young prince defeated the black drake <a title="Searinox" href="http://wowwiki.wikia.com/wiki/Searinox">Searinox</a> to retrieve its heart for the dwarf
                                <a title="Feranor Steeltoe" href="http://wowwiki.wikia.com/wiki/Feranor_Steeltoe">Feranor Steeltoe</a> to forge into an orb of fire. Arthas used this magical item to kill the <a class="mw-redirect" title="Blackrock Clan" href="http://wowwiki.wikia.com/wiki/Blackrock_Clan">Blackrock Clan&#8217;s</a>blademaster leading the raids.</p>
                            <div class="freya-read-more"><a class="more-link" href="index.html@p=4752">Continue reading</a></div>
                        </div>

                        <div class="bottomBlog">


                            <div class="blog_social"> Share:
                                <div class="addthis_toolbox">
                                    <div class="custom_images">
                                        <a class="addthis_button_facebook" addthis:url="http://freya.premiumcoding.com/the-story-of-arthas-menethil/" addthis:title="The Story of Arthas Menethil"><i class="fa fa-facebook"></i></a><a class="addthis_button_twitter" addthis:url="http://freya.premiumcoding.com/the-story-of-arthas-menethil/" addthis:title="The Story of Arthas Menethil"><i class="fa fa-twitter"></i></a><a class="addthis_button_pinterest_share" addthis:url="http://freya.premiumcoding.com/the-story-of-arthas-menethil/" addthis:title="The Story of Arthas Menethil"><i class="fa fa-pinterest"></i></a><a class="addthis_button_google_plusone_share" addthis:url="http://freya.premiumcoding.com/the-story-of-arthas-menethil/" g:plusone:count="false" addthis:title="The Story of Arthas Menethil"><i class="fa fa-google-plus"></i></a><a class="addthis_button_stumbleupon" addthis:url="http://freya.premiumcoding.com/the-story-of-arthas-menethil/" addthis:title="The Story of Arthas Menethil"><i class="fa fa-stumbleupon"></i></a>
                                    </div>
                                    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js"></script>
                                </div>
                            </div>
                            <!-- end of socials -->

                            <div class="blog_time_read">
                                Reading time: 7 min
                            </div>
                            <!-- end of reading -->
                        </div>

                        <!-- end of bottom blog -->
                    </div>


                </div>
            </div>
        </div>

        <div class="blogpostcategory">
            <div class="topBlog">

                <h2 class="title"><a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">Nordic battle</a></h2>
                <div class="post-meta">
                    <a class="post-meta-time" href="2014/06/06/index.html">June 6, 2014</a> <a class="post-meta-author" href="index.html">by Freyja</a> <a href="index.html@p=2501#commentform">6 Comments</a>
                </div>
                <!-- end of post meta -->
            </div>


            <a class="overdefultlink" href="index.html@p=2501">
                <div class="overdefult"></div>
            </a>

            <div class="blogimage">
                <div class="loading"></div>
                <a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle"><img width="1180" height="636" src="{{asset('assets/img/freyja-image-1-1180x636.jpg')}}" class="attachment-freya-postBlock size-freya-postBlock wp-post-image" alt="freyja-image-1" /></a>
            </div>

            <div class="entry">
                <div class="meta">
                    <div class="blogContent">
                        <div class="blogcontent"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsanet iusto odio dignissim qui blandit praesent. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim
                                placerat facer possim assum.</p>
                            <div class="su-quote su-quote-style-default su-quote-has-cite">
                                <div class="su-quote-inner su-clearfix"><span class="su-quote-cite">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna Veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </span></div>
                            </div>
                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. <strong>Nam liber tempor</strong> cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit.</p>
                            <p>Duis autem vel eum iriure dolor in hendrerit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsanet iusto odio dignissim qui blandit praesent. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                            <div class="freya-read-more"><a class="more-link" href="index.html@p=2501">Continue reading</a></div>
                        </div>

                        <div class="bottomBlog">


                            <div class="blog_social"> Share:
                                <div class="addthis_toolbox">
                                    <div class="custom_images">
                                        <a class="addthis_button_facebook" addthis:url="http://freya.premiumcoding.com/nordic-battle/" addthis:title="Nordic battle"><i class="fa fa-facebook"></i></a><a class="addthis_button_twitter" addthis:url="http://freya.premiumcoding.com/nordic-battle/" addthis:title="Nordic battle"><i class="fa fa-twitter"></i></a><a class="addthis_button_pinterest_share" addthis:url="http://freya.premiumcoding.com/nordic-battle/" addthis:title="Nordic battle"><i class="fa fa-pinterest"></i></a><a class="addthis_button_google_plusone_share" addthis:url="http://freya.premiumcoding.com/nordic-battle/" g:plusone:count="false" addthis:title="Nordic battle"><i class="fa fa-google-plus"></i></a><a class="addthis_button_stumbleupon" addthis:url="http://freya.premiumcoding.com/nordic-battle/" addthis:title="Nordic battle"><i class="fa fa-stumbleupon"></i></a>
                                    </div>
                                    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js"></script>
                                </div>
                            </div>
                            <!-- end of socials -->

                            <div class="blog_time_read">
                                Reading time: 5 min
                            </div>
                            <!-- end of reading -->
                        </div>

                        <!-- end of bottom blog -->
                    </div>


                </div>
            </div>
        </div>


        <div class="blogpostcategory">
            <div class="topBlog">

                <h2 class="title"><a href="index.html@p=6100" rel="bookmark" title="Permanent Link to Ancient halls of Valhalla">Ancient halls of Valhalla</a></h2>
                <div class="post-meta">
                    <a class="post-meta-time" href="2013/01/03/index.html">January 3, 2013</a> <a class="post-meta-author" href="index.html">by Freyja</a> <a href="index.html@p=6100#commentform">No Comments</a>
                </div>
                <!-- end of post meta -->
            </div>


            <a class="overdefultlink" href="index.html@p=6100">
                <div class="overdefult"></div>
            </a>

            <div class="blogimage">
                <div class="loading"></div>
                <a href="index.html@p=6100" rel="bookmark" title="Permanent Link to Ancient halls of Valhalla"><img width="1180" height="681" src="{{asset('assets/img/freyja-image-13-1180x681.jpg')}}" class="attachment-freya-postBlock size-freya-postBlock wp-post-image" alt="freyja-image-13" /></a>
            </div>

            <div class="entry">
                <div class="meta">
                    <div class="blogContent">
                        <div class="blogcontent"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsanet iusto odio dignissim qui blandit praesent. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim
                                placerat facer possim assum.</p>
                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. <strong>Nam liber tempor</strong> cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit.</p>
                            <div class="freya-read-more"><a class="more-link" href="index.html@p=6100">Continue reading</a></div>
                        </div>

                        <div class="bottomBlog">


                            <div class="blog_social"> Share:
                                <div class="addthis_toolbox">
                                    <div class="custom_images">
                                        <a class="addthis_button_facebook" addthis:url="http://freya.premiumcoding.com/ancient-halls-of-valhalla/" addthis:title="Ancient halls of Valhalla"><i class="fa fa-facebook"></i></a><a class="addthis_button_twitter" addthis:url="http://freya.premiumcoding.com/ancient-halls-of-valhalla/" addthis:title="Ancient halls of Valhalla"><i class="fa fa-twitter"></i></a><a class="addthis_button_pinterest_share" addthis:url="http://freya.premiumcoding.com/ancient-halls-of-valhalla/" addthis:title="Ancient halls of Valhalla"><i class="fa fa-pinterest"></i></a><a class="addthis_button_google_plusone_share" addthis:url="http://freya.premiumcoding.com/ancient-halls-of-valhalla/" g:plusone:count="false" addthis:title="Ancient halls of Valhalla"><i class="fa fa-google-plus"></i></a><a class="addthis_button_stumbleupon" addthis:url="http://freya.premiumcoding.com/ancient-halls-of-valhalla/" addthis:title="Ancient halls of Valhalla"><i class="fa fa-stumbleupon"></i></a>
                                    </div>
                                    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js"></script>
                                </div>
                            </div>
                            <!-- end of socials -->

                            <div class="blog_time_read">
                                Reading time: 3 min
                            </div>
                            <!-- end of reading -->
                        </div>

                        <!-- end of bottom blog -->
                    </div>


                </div>
            </div>
        </div>


        <div class="wp-pagenavi">
            <span class="pages">Page 1 of 4</span><span class="current">1</span><a href="page/2/index.html" class="page" title="2">2</a><a href="page/3/index.html" class="page" title="3">3</a><a href="page/4/index.html" class="page" title="4">4</a><a href="page/2/index.html" class="nextpostslink">&raquo;</a></div>


    </div><!-- sidebar -->
    <div class="sidebar">
        <div class="widget category_posts"><h3>Popular Posts</h3>
            <div class="widget-line"></div>


            <div class="widgett">
                <div class="imgholder">
                    <a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">
                        <img src="{{asset('assets/img/freyja-image-1-220x150.jpg')}}" alt="Nordic battle" width="285" height="155">
                    </a>
                </div>
                <div class="wttitle"><h4><a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">Nordic battle</a></h4></div>
                <div class="widget-date">June 6, 2014</div>
            </div>


            <div class="widgett">
                <div class="imgholder">
                    <a href="index.html@p=1484" rel="bookmark" title="Permanent Link to Attack of the Dragon">
                        <img src="{{asset('assets/img/freyja-image-11-220x150.jpg')}}" alt="Attack of the Dragon" width="285" height="155">
                    </a>
                </div>
                <div class="wttitle"><h4><a href="index.html@p=1484" rel="bookmark" title="Permanent Link to Attack of the Dragon">Attack of the Dragon</a></h4></div>
                <div class="widget-date">May 23, 2011</div>
            </div>


            <div class="widgett">
                <div class="imgholder">
                    <a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">
                        <img src="{{asset('assets/img/freyja-image-16-220x150.jpg')}}" alt="The Story of Arthas Menethil" width="285" height="155">
                    </a>
                </div>
                <div class="wttitle"><h4><a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">The Story of Arthas Menethil</a></h4></div>
                <div class="widget-date">November 22, 2016</div>
            </div>


            <div class="widgett">
                <div class="imgholder">
                    <a href="index.html@p=4943" rel="bookmark" title="Permanent Link to This Is A Gallery Post">
                        <img src="{{asset('assets/img/world_of_warcraft_dragon-wallpaper-1280x800-220x150.jpg')}}" alt="This Is A Gallery Post" width="285" height="155">
                    </a>
                </div>
                <div class="wttitle"><h4><a href="index.html@p=4943" rel="bookmark" title="Permanent Link to This Is A Gallery Post">This Is A Gallery Post</a></h4></div>
                <div class="widget-date">April 19, 2011</div>
            </div>


        </div>
        <div class="widget widget_categories"><h3>Categories</h3>
            <div class="widget-line"></div>
            <ul>
                <li class="cat-item cat-item-74"><a href="category/characters/index.html">Characters</a>
                </li>
                <li class="cat-item cat-item-3"><a href="category/editors-choice/index.html">Editor&#039;s choice</a>
                </li>
                <li class="cat-item cat-item-76"><a href="category/events/index.html">Events</a>
                </li>
                <li class="cat-item cat-item-5"><a href="category/front-page-slideshow/index.html">Front Page Slideshow</a>
                </li>
                <li class="cat-item cat-item-73"><a href="category/wars/index.html">Wars</a>
                </li>
            </ul>
        </div>
        <div class="widget recent_posts"><h3>Recent Posts</h3>
            <div class="widget-line"></div>


            <div class="widgett">
                <div class="imgholder">
                    <a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">
                        <img src="{{asset('assets/img/freyja-image-16-220x150.jpg')}}" alt="The Story of Arthas Menethil" width="285" height="155">
                    </a>
                </div>
                <div class="wttitle"><h4><a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">The Story of Arthas Menethil</a></h4></div>
                <div class="widget-date">November 22, 2016</div>


            </div>


            <div class="widgett">
                <div class="imgholder">
                    <a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">
                        <img src="{{asset('assets/img/freyja-image-1-220x150.jpg')}}" alt="Nordic battle" width="285" height="155">
                    </a>
                </div>
                <div class="wttitle"><h4><a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">Nordic battle</a></h4></div>
                <div class="widget-date">June 6, 2014</div>


            </div>


            <div class="widgett">
                <div class="imgholder">
                    <a href="index.html@p=6100" rel="bookmark" title="Permanent Link to Ancient halls of Valhalla">
                        <img src="{{asset('assets/img/freyja-image-13-220x150.jpg')}}" alt="Ancient halls of Valhalla" width="285" height="155">
                    </a>
                </div>
                <div class="wttitle"><h4><a href="index.html@p=6100" rel="bookmark" title="Permanent Link to Ancient halls of Valhalla">Ancient halls of Valhalla</a></h4></div>
                <div class="widget-date">January 3, 2013</div>


            </div>


            <div class="widgett">
                <div class="imgholder">
                    <a href="index.html@p=6198" rel="bookmark" title="Permanent Link to Winter is here">
                        <img src="{{asset('assets/img/freyja-image-6-220x150.jpg')}}" alt="Winter is here" width="285" height="155">
                    </a>
                </div>
                <div class="wttitle"><h4><a href="index.html@p=6198" rel="bookmark" title="Permanent Link to Winter is here">Winter is here</a></h4></div>
                <div class="widget-date">December 4, 2012</div>


            </div>


        </div>
        <div class="widget widget_fbw_id"><h3>Like Us On Facebook</h3>
            <div class="widget-line"></div>
            <center>
                <div class="loader"><img src="{{asset('assets/img/loader.gif')}}" /></div>
            </center>
            <div id="fb-root"></div>
            <div class="fb-page" data-href="http://facebook.com/PremiumCoding" data-width="285" data-height="350" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true" data-show-posts="true" style=""></div>
        </div>
        <div class="widget widget_wysija"><h3>Subscribe to our Newsletter</h3>
            <div class="widget-line"></div>
            <div class="widget_wysija_cont">
                <div id="msg-form-wysija-3" class="wysija-msg ajax"></div>
                <form id="form-wysija-3" method="post" action="index.html#wysija" class="widget_wysija">

                    Sign up to receive updates and join our 1 subscribers that see what's new with my Freyja's Blog!
                    <p class="wysija-paragraph">


                        <input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Email" value="" />



    <span class="abs-req">
        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
    </span>

                    </p>
                    <input class="wysija-submit wysija-submit-field" type="submit" value="Subscribe!" />

                    <input type="hidden" name="form_id" value="1" />
                    <input type="hidden" name="action" value="save" />
                    <input type="hidden" name="controller" value="subscribers" />
                    <input type="hidden" value="1" name="wysija-page" />


                    <input type="hidden" name="wysija[user_list][list_ids]" value="1" />

                </form>
            </div>
        </div>
        <div class="widget widget_tag_cloud"><h3>Tag Cloud</h3>
            <div class="widget-line"></div>
            <div class="tagcloud"><a href='tag/ancient/index.html' class='tag-link-83 tag-link-position-1' title='1 topic' style='font-size: 8pt;'>ancient</a>
                <a href='tag/arthas/index.html' class='tag-link-78 tag-link-position-2' title='1 topic' style='font-size: 8pt;'>arthas</a>
                <a href='tag/arthas-manethil/index.html' class='tag-link-92 tag-link-position-3' title='1 topic' style='font-size: 8pt;'>Arthas Manethil</a>
                <a href='tag/arthas-menethil/index.html' class='tag-link-79 tag-link-position-4' title='1 topic' style='font-size: 8pt;'>Arthas Menethil</a>
                <a href='tag/attack/index.html' class='tag-link-94 tag-link-position-5' title='1 topic' style='font-size: 8pt;'>Attack</a>
                <a href='tag/audio-2/index.html' class='tag-link-12 tag-link-position-6' title='1 topic' style='font-size: 8pt;'>audio</a>
                <a href='tag/battle/index.html' class='tag-link-80 tag-link-position-7' title='2 topics' style='font-size: 14.3pt;'>battle</a>
                <a href='tag/character/index.html' class='tag-link-93 tag-link-position-8' title='1 topic' style='font-size: 8pt;'>character</a>
                <a href='tag/characters/index.html' class='tag-link-90 tag-link-position-9' title='2 topics' style='font-size: 14.3pt;'>characters</a>
                <a href='tag/cold/index.html' class='tag-link-86 tag-link-position-10' title='1 topic' style='font-size: 8pt;'>cold</a>
                <a href='tag/dark/index.html' class='tag-link-88 tag-link-position-11' title='1 topic' style='font-size: 8pt;'>dark</a>
                <a href='tag/dragons/index.html' class='tag-link-95 tag-link-position-12' title='1 topic' style='font-size: 8pt;'>dragons</a>
                <a href='tag/gallery/index.html' class='tag-link-29 tag-link-position-13' title='1 topic' style='font-size: 8pt;'>gallery</a>
                <a href='tag/halls/index.html' class='tag-link-84 tag-link-position-14' title='1 topic' style='font-size: 8pt;'>halls</a>
                <a href='tag/moon/index.html' class='tag-link-87 tag-link-position-15' title='1 topic' style='font-size: 8pt;'>moon</a>
                <a href='tag/night/index.html' class='tag-link-89 tag-link-position-16' title='1 topic' style='font-size: 8pt;'>night</a>
                <a href='tag/nordic/index.html' class='tag-link-81 tag-link-position-17' title='1 topic' style='font-size: 8pt;'>nordic</a>
                <a href='tag/past/index.html' class='tag-link-91 tag-link-position-18' title='1 topic' style='font-size: 8pt;'>past</a>
                <a href='tag/post/index.html' class='tag-link-44 tag-link-position-19' title='1 topic' style='font-size: 8pt;'>post</a>
                <a href='tag/story/index.html' class='tag-link-77 tag-link-position-20' title='1 topic' style='font-size: 8pt;'>story</a>
                <a href='tag/valhalla/index.html' class='tag-link-85 tag-link-position-21' title='1 topic' style='font-size: 8pt;'>Valhalla</a>
                <a href='tag/video/index.html' class='tag-link-61 tag-link-position-22' title='1 topic' style='font-size: 8pt;'>video</a>
                <a href='tag/war/index.html' class='tag-link-82 tag-link-position-23' title='4 topics' style='font-size: 22pt;'>war</a>
                <a href='tag/wars/index.html' class='tag-link-96 tag-link-position-24' title='1 topic' style='font-size: 8pt;'>wars</a>
                <a href='tag/winter/index.html' class='tag-link-64 tag-link-position-25' title='1 topic' style='font-size: 8pt;'>winter</a></div>
        </div>
        <div class="widget videos"><h3>Freya Video</h3>
            <div class="widget-line"></div>
            <style scoped>
                .widget.videos .self.id-videos-widget-2 .wttitle {
                    margin-top: -200px
                }
            </style>
            <div class="widgett  id-videos-widget-2">
                <div class="video">
                    <iframe width="800" height="450" src="https://www.youtube.com/embed/BCr7y4SLhck?feature=oembed" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>


        </div>
        <div class="widget socials"><h3>Socialize with me</h3>
            <div class="widget-line"></div>
            <div class="widgett">
                <div class="social_icons">
                    <a target="_blank" href="http://twitter.com/PremiumCoding" title="Twitter"><i class="fa fa-twitter"></i></a><a target="_blank" href="https://www.facebook.com/PremiumCoding" title="Facebook"><i class="fa fa-facebook"></i></a><a target="_blank" href="https://dribbble.com/gljivec" title="Dribbble"><i class="fa fa-dribbble"></i></a><a target="_blank" href="https://www.flickr.com/" title="Flickr"><i class="fa fa-flickr"></i></a><a target="_blank" href="http://www.pinterest.com/gljivec/" title="Pinterest"><i class="fa fa-pinterest"></i></a><a target="_blank" href="index.html#" title="Google"><i class="fa fa-google-plus"></i></a><a target="_blank" href="https://www.instagram.com/dreamypixels/" title="Instagram"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
        <div class="widget category_select_slider_posts"><h3>Freya Post slider</h3>
            <div class="widget-line"></div>
            <div class="category_select_slider_posts-widget-4" style="float:left;">

                <ul class="bxslider-category_select_slider_posts-widget-4">

                    <li>
                        <div class="widgett">

                            <div class="imgholder">
                                <a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">
                                    <img width="220" height="150" src="{{asset('assets/img/freyja-image-16-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-16" />
                                </a>
                            </div>
                            <div class="wttitle"><h4><a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">The Story of Arthas Menethil</a></h4></div>
                        </div>
                    </li>


                    <li>
                        <div class="widgett">

                            <div class="imgholder">
                                <a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">
                                    <img width="220" height="150" src="{{asset('assets/img/freyja-image-1-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-1" />
                                </a>
                            </div>
                            <div class="wttitle"><h4><a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">Nordic battle</a></h4></div>
                        </div>
                    </li>


                    <li>
                        <div class="widgett">

                            <div class="imgholder">
                                <a href="index.html@p=6100" rel="bookmark" title="Permanent Link to Ancient halls of Valhalla">
                                    <img width="220" height="150" src="{{asset('assets/img/freyja-image-13-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-13" />
                                </a>
                            </div>
                            <div class="wttitle"><h4><a href="index.html@p=6100" rel="bookmark" title="Permanent Link to Ancient halls of Valhalla">Ancient halls of Valhalla</a></h4></div>
                        </div>
                    </li>


                    <li>
                        <div class="widgett">

                            <div class="imgholder">
                                <a href="index.html@p=6198" rel="bookmark" title="Permanent Link to Winter is here">
                                    <img width="220" height="150" src="{{asset('assets/img/freyja-image-6-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-6" />
                                </a>
                            </div>
                            <div class="wttitle"><h4><a href="index.html@p=6198" rel="bookmark" title="Permanent Link to Winter is here">Winter is here</a></h4></div>
                        </div>
                    </li>


                    <li>
                        <div class="widgett">

                            <div class="imgholder">
                                <a href="index.html@p=4174" rel="bookmark" title="Permanent Link to Arthas Manethil">
                                    <img width="220" height="150" src="{{asset('assets/img/arthas_menethil-wallpaper-1280x800-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="arthas_menethil-wallpaper-1280x800" />
                                </a>
                            </div>
                            <div class="wttitle"><h4><a href="index.html@p=4174" rel="bookmark" title="Permanent Link to Arthas Manethil">Arthas Manethil</a></h4></div>
                        </div>
                    </li>


                    <li>
                        <div class="widgett">

                            <div class="imgholder">
                                <a href="index.html@p=1484" rel="bookmark" title="Permanent Link to Attack of the Dragon">
                                    <img width="220" height="150" src="{{asset('assets/img/freyja-image-11-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-11" />
                                </a>
                            </div>
                            <div class="wttitle"><h4><a href="index.html@p=1484" rel="bookmark" title="Permanent Link to Attack of the Dragon">Attack of the Dragon</a></h4></div>
                        </div>
                    </li>

                </ul>
            </div>
            <input id="slider_id" type="hidden" value="category_select_slider_posts-widget-4">
            <input id="slider" type="hidden" value="2">

        </div>
    </div>
@endsection