<!DOCTYPE html>
<html lang="en-US" class="no-js">
    <!-- start -->
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="format-detection" content="telephone=no">

        <title>Freyja Blog - Stories about Nordic battles</title>

        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/", "svgExt": ".svg", "source": {"concatemoji": "http:\/\/freya.premiumcoding.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6.1"}};
            !function (a, b, c) {
                function d(a) {
                    var c, d, e, f, g, h = b.createElement("canvas"), i = h.getContext && h.getContext("2d"), j = String.fromCharCode;
                    if (!i || !i.fillText)return !1;
                    switch (i.textBaseline = "top", i.font = "600 32px Arial", a) {
                        case"flag":
                            return i.fillText(j(55356, 56806, 55356, 56826), 0, 0), !(h.toDataURL().length < 3e3) && (i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 65039, 8205, 55356, 57096), 0, 0), c = h.toDataURL(), i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 55356, 57096), 0, 0), d = h.toDataURL(), c !== d);
                        case"diversity":
                            return i.fillText(j(55356, 57221), 0, 0), e = i.getImageData(16, 16, 1, 1).data, f = e[0] + "," + e[1] + "," + e[2] + "," + e[3], i.fillText(j(55356, 57221, 55356, 57343), 0, 0), e = i.getImageData(16, 16, 1, 1).data, g = e[0] + "," + e[1] + "," + e[2] + "," + e[3], f !== g;
                        case"simple":
                            return i.fillText(j(55357, 56835), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0];
                        case"unicode8":
                            return i.fillText(j(55356, 57135), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0];
                        case"unicode9":
                            return i.fillText(j(55358, 56631), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0]
                    }
                    return !1
                }

                function e(a) {
                    var c = b.createElement("script");
                    c.src = a, c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
                }

                var f, g, h, i;
                for (i = Array("simple", "flag", "unicode8", "diversity", "unicode9"), c.supports = {everything: !0, everythingExceptFlag: !0}, h = 0; h < i.length; h++) {
                    c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]);
                }
                c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {c.DOMReady = !0}, c.supports.everything || (g = function () {c.readyCallback()}, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function () {"complete" === b.readyState && c.readyCallback()})), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
            }(window, document, window._wpemojiSettings);
        </script>
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link rel='stylesheet' id='validate-engine-css-css' href='{{asset('assets/css/validationEngine.jquery.css')}}' type='text/css' media='all' />
        <link rel='stylesheet' id='contact-form-7-css' href='{{asset('assets/css/styles.css')}}' type='text/css' media='all' />
        <link rel='stylesheet' id='sb_instagram_styles-css' href='{{asset('assets/css/sb-instagram.min.css')}}' type='text/css' media='all' />
        <link rel='stylesheet' id='sb_instagram_icons-css' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css?ver=4.6.3' type='text/css' media='all' />
        <link rel='stylesheet' id='tp_twitter_plugin_css-css' href='{{asset('assets/css/tp_twitter_plugin.css')}}' type='text/css' media='screen' />
        <link rel='stylesheet' id='rs-plugin-settings-css' href='{{asset('assets/css/settings.css')}}' type='text/css' media='all' />
        <style id='rs-plugin-settings-inline-css' type='text/css'>
            #rs-demo-id {
            }
        </style>
        <link rel='stylesheet' id='prettyphoto-css' href='{{asset('assets/css/prettyPhoto.css')}}' type='text/css' media='all' />
        <link rel='stylesheet' id='googleFonts-css' href='https://fonts.googleapis.com/css?family=Merriweather%3A400%2C700%7CArima+Madurai%3A400%2C700%7CArima+Madurai%3A400%2C700%7CArima+Madurai%3A400%2C700&#038;ver=4.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='style-css' href='{{asset('assets/css/style.css')}}' type='text/css' media='all' />
        <style id='style-inline-css' type='text/css'>

            @font-face {
                font-family: "text-gyre-adventor-r";
                src: url("wp-content/themes/freya/css/fonts/texgyreadventor-regular.woff") format("woff");
                font-style: normal;
                font-weight: normal;
            }

            @font-face {
                font-family: "text-gyre-adventor-i";
                src: url("wp-content/themes/freya/css/fonts/texgyreadventor-italic.woff") format("woff");
                font-style: italic;
                font-weight: normal;
            }

            @font-face {
                font-family: "text-gyre-adventor-b";
                src: url("wp-content/themes/freya/css/fonts/texgyreadventor-bold.woff") format("woff");
                font-style: normal;
                font-weight: bold;
            }

            .block_footer_text, .quote-category .blogpostcategory {
                font-family: Arima Madurai, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;
            }

            body {
                background: #F8F8F8 !important;
                color: #525452;
                font-family: Merriweather, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;
                font-size: 15px;
                font-weight: normal;
            }

            ::selection {
                background: #000;
                color: #fff;
                text-shadow: none;
            }

            h1, h2, h3, h4, h5, h6, .block1 p, .hebe .tp-tab-desc {
                font-weight: normal;
                font-style: normal;
                font-family: Arima Madurai, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;
            }

            h1 {
                color: #333;
                font-size: 40px !important;
            }

            h2, .term-description p {
                color: #333;
                font-size: 36px !important;
            }

            h3 {
                color: #333;
                font-size: 30px !important;
            }

            h4 {
                color: #333;
                font-size: 26px !important;
            }

            h5 {
                color: #333;
                font-size: 22px !important;
            }

            h6 {
                color: #333;
                font-size: 18px !important;
            }

            .top-wrapper .social_icons a i, .top-wrapper .widget_search form input#s {
                color: #ffffff;
            }

            i.fa.fa-search.search-desktop {
                color: #ffffff !important;
            }

            .pagenav a {
                font-style: normal;
                font-family: Arima Madurai !important;
                font-size: 13px;
                font-weight: normal;
                color: #ffffff;
            }

            .block1_lower_text p, .widget_wysija_cont .updated, .widget_wysija_cont .login .message, p.edd-logged-in, #edd_login_form, #edd_login_form p {
                font-family: Merriweather, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif !important;
                color: #444;
                font-size: 14px;
            }

            a, select, input, textarea, button {
                color: #343434;
            }

            h3#reply-title, select, input, textarea, button, .link-category .title a, .wttitle h4 a {
                font-family: Merriweather, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;
            }

            .block2_text p, .su-quote-has-cite span {
                font-family: Arima Madurai, "Helvetica Neue", Arial, Helvetica, Verdana, sans-serif;
            }

            /* ***********************
            --------------------------------------
            ------------MAIN COLOR----------
            --------------------------------------
            *********************** */

            a:hover, span, .current-menu-item a, .blogmore, .more-link, .pagenav.fixedmenu li a:hover, .widget ul li a:hover, .pagenav.fixedmenu li.current-menu-item > a, .block2_text a,
            .blogcontent a, .sentry a, .post-meta a:hover, .sidebar .social_icons i:hover, .blog_social .addthis_toolbox a:hover, .addthis_toolbox a:hover, .content.blog .single-date, a.post-meta-author, .block1_text p,
            .grid .blog-category a, .pmc-main-menu li.colored a, .top-wrapper .social_icons a i:hover, #footer a:hover, .copyright a, .footer-social a span:hover, .footer-social a:hover {
                color: #109ed0;
            }

            #footer a:hover {
                color: #109ed0 !important;
            }

            .su-quote-style-default {
                border-left: 5px solid #109ed0;
            }

            .addthis_toolbox a i:hover {
                color: #109ed0 !important;
            }

            /* ***********************
            --------------------------------------
            ------------BACKGROUND MAIN COLOR----------
            --------------------------------------
            *********************** */

            .top-cart, .widget_tag_cloud a:hover, .sidebar .widget_search #searchsubmit,
            .specificComment .comment-reply-link:hover, #submit:hover, .wpcf7-submit:hover, #submit:hover,
            .link-title-previous:hover, .link-title-next:hover, .specificComment .comment-edit-link:hover, .specificComment .comment-reply-link:hover, h3#reply-title small a:hover, .pagenav li a:after,
            .widget_wysija_cont .wysija-submit, .widget ul li:before, #footer .widget_search #searchsubmit, .blogpost .tags a:hover,
            .mainwrap.single-default.sidebar .link-title-next:hover, .mainwrap.single-default.sidebar .link-title-previous:hover, .top-search-form i:hover, .edd-submit.button.blue:hover,
            ul#menu-top-menu, a.catlink:hover, #commentform #submit, input[type="submit"] {
                background: #109ed0;
            }

            .pagenav li li a:hover {
                background: none;
            }

            .edd-submit.button.blue:hover, .cart_item.edd_checkout a:hover {
                background: #109ed0 !important;
            }

            .link-title-previous:hover, .link-title-next:hover {
                color: #fff;
            }

            #headerwrap {
                background: #101010;
            }

            #freya-slider-wrapper, .freya-rev-slider {
                padding-top: 0px;
            }

            /* ***********************
           --------------------------------------
           ------------BOXED---------------------
           -----------------------------------*/
            .sidebars-wrap.bottom {
                margin-top: 0px !important;
                padding-bottom: 75px;
            }

            .top-wrapper {
                background: #1b1b1b;
            }

            .pagenav {
                background: #101010;
                border-top: 0px solid #000;
                border-bottom: 0px solid #000;
            }

            /* ***********************
            --------------------------------------
            ------------CUSTOM CSS----------
            --------------------------------------
            *********************** */


        </style>
        <link rel='stylesheet' id='easy-social-share-buttons-css' href='{{asset('assets/css/easy-social-share-buttons.min.css')}}' type='text/css' media='all' />
        <link rel='stylesheet' id='essb-social-followers-counter-css' href='{{asset('assets/css/essb-followers-counter.min.css')}}' type='text/css' media='all' />
        <script type='text/javascript' src='{{asset('assets/js/jquery.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery-migrate.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.themepunch.tools.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.themepunch.revolution.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/webfontloader.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/custom.js')}}'></script>
        <script type='text/javascript'>

            "use strict";
            jQuery(document).ready(function ($) {
                jQuery(".searchform #s").attr("value", "Search and hit enter...");
                jQuery(".searchform #s").focus(function () {
                    jQuery(".searchform #s").val("");
                });

                jQuery(".searchform #s").focusout(function () {
                    if (jQuery(".searchform #s").attr("value") == "")
                        jQuery(".searchform #s").attr("value", "Search and hit enter...");
                });

            });


            jQuery(document).ready(function ($) {
                jQuery(".bxslider").bxSlider({
                    easing:     "easeInOutQuint",
                    captions:   true,
                    speed:      800,
                    buildPager: function (slideIndex) {
                        switch (slideIndex) {

                        }
                    }
                });
            });
        </script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.bxslider.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/pmc_infinity.js')}}'></script>
        <script type='text/javascript' src='https://use.fontawesome.com/30ede005b9.js'></script>

        <script type="text/javascript">var essb_settings = {"ajax_url": "http:\/\/freya.premiumcoding.com\/wp-admin\/admin-ajax.php", "essb3_nonce": "7d357f356b", "essb3_plugin_url": "http:\/\/freya.premiumcoding.com\/wp-content\/plugins\/easy-social-share-buttons3", "essb3_facebook_total": true, "essb3_admin_ajax": false, "essb3_internal_counter": false, "essb3_stats": false, "essb3_ga": false, "essb3_ga_mode": "simple", "essb3_counter_button_min": 0, "essb3_counter_total_min": 0, "blog_url": "http:\/\/freya.premiumcoding.com\/", "ajax_type": "wp", "essb3_postfloat_stay": false, "essb3_no_counter_mailprint": false, "essb3_single_ajax": false, "twitter_counter": "self", "post_id": 4752};</script>
        <meta name="generator" content="Powered by Slider Revolution 5.2.6 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
        <meta name="msapplication-TileImage" content="http://freya.premiumcoding.com/wp-content/uploads/2016/11/Freyja-favicon.png" />
    </head>
    <!-- start body -->


    <body class="home blog">
        <!-- start header -->
        <!-- fixed menu -->


        <div class="pagenav fixedmenu">
            <div class="holder-fixedmenu">
                <div class="logo-fixedmenu">
                    <a href="#"><img src="{{asset('assets/img/Freyja-logo-1.png')}}" alt="Freyja Blog - Stories about Nordic battles"></a>
                </div>
                <div class="menu-fixedmenu home">
                    <ul id="menu-main-menu" class="menu">
                        <li id="menu-item-6992-6462" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home"><a href="index.html">Home</a></li>
                        <li id="menu-item-6319-6465" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="index.html#">Features</a>
                            <ul class="sub-menu">
                                <li id="menu-item-4932-6466" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="index.html@p=4752">Standard Post</a></li>
                                <li id="menu-item-5224-6467" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="index.html@p=4943">Gallery Post</a></li>
                                <li id="menu-item-412-6468" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="index.html@p=3499">Video Post</a></li>
                                <li id="menu-item-603-6469" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="index.html@p=6343">Audio Post</a></li>
                                <li id="menu-item-9609-6540" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6415">Sidebar page with Slideshow</a></li>
                                <li id="menu-item-8982-6541" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6413">Full width page with Slideshow</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-8058-6623" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="category/wars/index.html">Wars</a></li>
                        <li id="menu-item-1977-6624" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="category/characters/index.html">Characters</a></li>
                        <li id="menu-item-8952-6625" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="category/events/index.html">Events</a></li>
                        <li id="menu-item-4281-6463" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="index.html#">Pages</a>
                            <ul class="sub-menu">
                                <li id="menu-item-7881-6546" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6209">Sample Page Example</a></li>
                                <li id="menu-item-6155-6544" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6215">Typography</a></li>
                                <li id="menu-item-3582-6542" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6415">Page with Sidebar</a></li>
                                <li id="menu-item-2227-6543" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6413">Full width page</a></li>
                                <li id="menu-item-3001-6547" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6238">About Us</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-8771-6461" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="category/editors-choice/index.html">Editor&#8217;s choice</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <header>
            <!-- top bar -->
            <div class="top-wrapper">
                <div class="top-wrapper-content">
                    <div class="top-left">
                        <div class="widget socials">
                            <div class="widgett">
                                <div class="social_icons">
                                    <a target="_blank" href="http://twitter.com/PremiumCoding" title="Twitter"><i class="fa fa-twitter"></i></a><a target="_blank" href="https://www.facebook.com/PremiumCoding" title="Facebook"><i class="fa fa-facebook"></i></a><a target="_blank" href="https://dribbble.com/gljivec" title="Dribbble"><i class="fa fa-dribbble"></i></a><a target="_blank" href="https://www.flickr.com/" title="Flickr"><i class="fa fa-flickr"></i></a><a target="_blank" href="http://www.pinterest.com/gljivec/" title="Pinterest"><i class="fa fa-pinterest"></i></a><a target="_blank" href="index.html#" title="Google"><i class="fa fa-google-plus"></i></a><a target="_blank" href="https://www.instagram.com/dreamypixels/" title="Instagram"><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="top-right">
                        <div class="widget widget_search">
                            <form method="get" id="searchform" class="searchform" action="index.html">
                                <input type="text" value="" name="s" id="s" />
                                <i class="fa fa-search search-desktop"></i>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="headerwrap">
                <!-- logo and main menu -->
                <div id="header">
                    <!-- respoonsive menu main-->
                    <!-- respoonsive menu no scrool bar -->
                    <div class="respMenu noscroll">
                        <div class="resp_menu_button"><i class="fa fa-list-ul fa-2x"></i></div>
                        <div class="menu-main-menu-container">
                            <div class="event-type-selector-dropdown"><a class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home" href="index.html"><strong>Home</strong></a><br>
                                <a class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" href="index.html#"><strong>Features</strong></a><br>

                                <a class="menu-item menu-item-type-custom menu-item-object-custom" href="index.html@p=4752">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Standard Post</a><br>
                                <a class="menu-item menu-item-type-custom menu-item-object-custom" href="index.html@p=4943">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Gallery Post</a><br>
                                <a class="menu-item menu-item-type-custom menu-item-object-custom" href="index.html@p=3499">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Video Post</a><br>
                                <a class="menu-item menu-item-type-custom menu-item-object-custom" href="index.html@p=6343">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Audio Post</a><br>
                                <a class="menu-item menu-item-type-post_type menu-item-object-page" href="index.html@p=6415">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Sidebar page with Slideshow</a><br>
                                <a class="menu-item menu-item-type-post_type menu-item-object-page" href="index.html@p=6413">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Full width page with Slideshow</a><br>


                                <a class="menu-item menu-item-type-taxonomy menu-item-object-category" href="category/wars/index.html"><strong>Wars</strong></a><br>
                                <a class="menu-item menu-item-type-taxonomy menu-item-object-category" href="category/characters/index.html"><strong>Characters</strong></a><br>
                                <a class="menu-item menu-item-type-taxonomy menu-item-object-category" href="category/events/index.html"><strong>Events</strong></a><br>
                                <a class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children" href="index.html#"><strong>Pages</strong></a><br>

                                <a class="menu-item menu-item-type-post_type menu-item-object-page" href="index.html@p=6209">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Sample Page Example</a><br>
                                <a class="menu-item menu-item-type-post_type menu-item-object-page" href="index.html@p=6215">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Typography</a><br>
                                <a class="menu-item menu-item-type-post_type menu-item-object-page" href="index.html@p=6415">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Page with Sidebar</a><br>
                                <a class="menu-item menu-item-type-post_type menu-item-object-page" href="index.html@p=6413">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>Full width page</a><br>
                                <a class="menu-item menu-item-type-post_type menu-item-object-page" href="index.html@p=6238">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>About Us</a><br>


                                <a class="menu-item menu-item-type-taxonomy menu-item-object-category" href="category/editors-choice/index.html"><strong>Editor's choice</strong></a><br>
                            </div>
                        </div>
                    </div>
                    <!-- logo -->
                    <div class="logo-inner">
                        <div id="logo" class="">
                            <a href="index.html"><img src="{{asset('assets/img/Freyja-logo.png')}}" alt="Freyja Blog - Stories about Nordic battles" /></a>
                        </div>

                    </div>

                    <!-- main menu -->
                    <div class="pagenav">
                        <div class="pmc-main-menu">
                            <ul id="menu-main-menu-container" class="menu">
                                <li id="menu-item-4570-6462" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home"><a href="index.html">Home</a></li>
                                <li id="menu-item-6186-6465" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="index.html#">Features</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-6033-6466" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="index.html@p=4752">Standard Post</a></li>
                                        <li id="menu-item-2059-6467" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="index.html@p=4943">Gallery Post</a></li>
                                        <li id="menu-item-8730-6468" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="index.html@p=3499">Video Post</a></li>
                                        <li id="menu-item-1801-6469" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="index.html@p=6343">Audio Post</a></li>
                                        <li id="menu-item-5286-6540" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6415">Sidebar page with Slideshow</a></li>
                                        <li id="menu-item-6639-6541" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6413">Full width page with Slideshow</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-9906-6623" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="category/wars/index.html">Wars</a></li>
                                <li id="menu-item-34-6624" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="category/characters/index.html">Characters</a></li>
                                <li id="menu-item-1119-6625" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="category/events/index.html">Events</a></li>
                                <li id="menu-item-7671-6463" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="index.html#">Pages</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-860-6546" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6209">Sample Page Example</a></li>
                                        <li id="menu-item-8112-6544" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6215">Typography</a></li>
                                        <li id="menu-item-3991-6542" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6415">Page with Sidebar</a></li>
                                        <li id="menu-item-5792-6543" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6413">Full width page</a></li>
                                        <li id="menu-item-3337-6547" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="index.html@p=6238">About Us</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-4403-6461" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="category/editors-choice/index.html">Editor&#8217;s choice</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="freya-slider-wrapper">
                <div id="freya-slider">
                    <link href="http://fonts.googleapis.com/css?family=Roboto+Slab%3A400%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                    <link href="http://fonts.googleapis.com/css?family=Roboto%3A900%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                    <link href="http://fonts.googleapis.com/css?family=Merriweather%3A400%2C900%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                        <!-- START REVOLUTION SLIDER 5.2.6 fullwidth mode -->
                        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.2.6">
                            <ul>    <!-- SLIDE  -->
                                <li data-index="rs-4752" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="http://freya.premiumcoding.com/wp-content/uploads/2016/03/freyja-image-16-100x50.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="The Story of Arthas Menethil" data-param1="Editor&#039;s choice,Even" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="{{asset('assets/img/dummy.png')}}" alt="" title="freyja-image-16" width="1440" height="995" data-lazyload="http://freya.premiumcoding.com/wp-content/uploads/2016/03/freyja-image-16.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->

                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme" id="slide-4752-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;"

                                         data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on"


                                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 1.00);"></div>

                                    <!-- LAYER NR. 2 -->
                                    <div class="tp-caption Newspaper-Title   tp-resizeme" id="slide-4752-layer-1" data-x="['center','left','left','left']" data-hoffset="['0','50','50','30']" data-y="['top','top','top','top']" data-voffset="['207','135','105','130']" data-fontsize="['40','50','50','30']" data-lineheight="['55','55','55','35']" data-fontweight="['500','400','400','400']" data-width="['751','600','600','420']" data-height="['70','none','none','none']" data-whitespace="normal" data-transform_idle="o:1;"

                                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on"


                                         style="z-index: 6; min-width: 751px; max-width: 751px; max-width: 70px; max-width: 70px; white-space: normal; font-size: 40px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Arima Madurai ;text-transform:uppercase;">The Story of Arthas Menethil
                                    </div>

                                    <!-- LAYER NR. 3 -->
                                    <div class="tp-caption Newspaper-Subtitle   tp-resizeme" id="slide-4752-layer-2" data-x="['center','left','left','left']" data-hoffset="['0','50','50','30']" data-y="['top','top','top','top']" data-voffset="['187','110','80','100']" data-fontsize="['12','15','15','15']" data-fontweight="['400','900','900','900']" data-color="['rgba(255, 255, 255, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)']" data-width="['158','none','none','none']" data-height="['41','none','none','none']" data-whitespace="['normal','nowrap','nowrap','nowrap']" data-transform_idle="o:1;"

                                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on"


                                         style="z-index: 7; min-width: 158px; max-width: 158px; max-width: 41px; max-width: 41px; white-space: normal; font-size: 12px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Merriweather;text-transform:uppercase;">
                                        November 22, 2016
                                    </div>

                                    <!-- LAYER NR. 4 -->
                                    <a class="tp-caption Newspaper-Button rev-btn " href="index.html@p=4752" target="_self" id="slide-4752-layer-5" data-x="['center','left','left','left']" data-hoffset="['0','53','53','30']" data-y="['top','top','top','top']" data-voffset="['280','331','301','245']" data-fontsize="['12','13','13','13']" data-fontweight="['400','700','700','700']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;" data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(16, 158, 208, 0.80);"

                                       data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="on" data-responsive="off"

                                       style="z-index: 8; white-space: nowrap; font-size: 12px; font-weight: 400;font-family:Merriweather;background-color:rgba(16, 158, 208, 1.00);padding:10px 30px 10px 30px;border-color:rgba(255, 255, 255, 0);border-style:solid;border-width:2px;border-radius:2px 2px 2px 2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">READ MORE </a>
                                </li>
                                <!-- SLIDE  -->
                                <li data-index="rs-2501" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="http://freya.premiumcoding.com/wp-content/uploads/2014/06/freyja-image-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Nordic battle" data-param1="Editor&#039;s choice,Even" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="{{asset('assets/img/dummy.png')}}" alt="" title="freyja-image-1" width="1280" height="636" data-lazyload="http://freya.premiumcoding.com/wp-content/uploads/2014/06/freyja-image-1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->

                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme" id="slide-2501-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;"

                                         data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on"


                                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 1.00);"></div>

                                    <!-- LAYER NR. 2 -->
                                    <div class="tp-caption Newspaper-Title   tp-resizeme" id="slide-2501-layer-1" data-x="['center','left','left','left']" data-hoffset="['0','50','50','30']" data-y="['top','top','top','top']" data-voffset="['207','135','105','130']" data-fontsize="['40','50','50','30']" data-lineheight="['55','55','55','35']" data-fontweight="['500','400','400','400']" data-width="['751','600','600','420']" data-height="['70','none','none','none']" data-whitespace="normal" data-transform_idle="o:1;"

                                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on"


                                         style="z-index: 6; min-width: 751px; max-width: 751px; max-width: 70px; max-width: 70px; white-space: normal; font-size: 40px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Arima Madurai ;text-transform:uppercase;">Nordic battle
                                    </div>

                                    <!-- LAYER NR. 3 -->
                                    <div class="tp-caption Newspaper-Subtitle   tp-resizeme" id="slide-2501-layer-2" data-x="['center','left','left','left']" data-hoffset="['0','50','50','30']" data-y="['top','top','top','top']" data-voffset="['187','110','80','100']" data-fontsize="['12','15','15','15']" data-fontweight="['400','900','900','900']" data-color="['rgba(255, 255, 255, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)']" data-width="['158','none','none','none']" data-height="['41','none','none','none']" data-whitespace="['normal','nowrap','nowrap','nowrap']" data-transform_idle="o:1;"

                                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on"


                                         style="z-index: 7; min-width: 158px; max-width: 158px; max-width: 41px; max-width: 41px; white-space: normal; font-size: 12px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Merriweather;text-transform:uppercase;">
                                        June 6, 2014
                                    </div>

                                    <!-- LAYER NR. 4 -->
                                    <a class="tp-caption Newspaper-Button rev-btn " href="index.html@p=2501" target="_self" id="slide-2501-layer-5" data-x="['center','left','left','left']" data-hoffset="['0','53','53','30']" data-y="['top','top','top','top']" data-voffset="['280','331','301','245']" data-fontsize="['12','13','13','13']" data-fontweight="['400','700','700','700']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;" data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(16, 158, 208, 0.80);"

                                       data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="on" data-responsive="off"

                                       style="z-index: 8; white-space: nowrap; font-size: 12px; font-weight: 400;font-family:Merriweather;background-color:rgba(16, 158, 208, 1.00);padding:10px 30px 10px 30px;border-color:rgba(255, 255, 255, 0);border-style:solid;border-width:2px;border-radius:2px 2px 2px 2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">READ MORE </a>
                                </li>
                                <!-- SLIDE  -->
                                <li data-index="rs-6100" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="http://freya.premiumcoding.com/wp-content/uploads/2013/01/freyja-image-13-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Ancient halls of Valhalla" data-param1="Editor&#039;s choice,Even" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="{{asset('assets/img/dummy.png')}}" alt="" title="freyja-image-13" width="1600" height="681" data-lazyload="http://freya.premiumcoding.com/wp-content/uploads/2013/01/freyja-image-13.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->

                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme" id="slide-6100-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;"

                                         data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on"


                                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 1.00);"></div>

                                    <!-- LAYER NR. 2 -->
                                    <div class="tp-caption Newspaper-Title   tp-resizeme" id="slide-6100-layer-1" data-x="['center','left','left','left']" data-hoffset="['0','50','50','30']" data-y="['top','top','top','top']" data-voffset="['207','135','105','130']" data-fontsize="['40','50','50','30']" data-lineheight="['55','55','55','35']" data-fontweight="['500','400','400','400']" data-width="['751','600','600','420']" data-height="['70','none','none','none']" data-whitespace="normal" data-transform_idle="o:1;"

                                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on"


                                         style="z-index: 6; min-width: 751px; max-width: 751px; max-width: 70px; max-width: 70px; white-space: normal; font-size: 40px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Arima Madurai ;text-transform:uppercase;">Ancient halls of Valhalla
                                    </div>

                                    <!-- LAYER NR. 3 -->
                                    <div class="tp-caption Newspaper-Subtitle   tp-resizeme" id="slide-6100-layer-2" data-x="['center','left','left','left']" data-hoffset="['0','50','50','30']" data-y="['top','top','top','top']" data-voffset="['187','110','80','100']" data-fontsize="['12','15','15','15']" data-fontweight="['400','900','900','900']" data-color="['rgba(255, 255, 255, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)']" data-width="['158','none','none','none']" data-height="['41','none','none','none']" data-whitespace="['normal','nowrap','nowrap','nowrap']" data-transform_idle="o:1;"

                                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on"


                                         style="z-index: 7; min-width: 158px; max-width: 158px; max-width: 41px; max-width: 41px; white-space: normal; font-size: 12px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Merriweather;text-transform:uppercase;">
                                        January 3, 2013
                                    </div>

                                    <!-- LAYER NR. 4 -->
                                    <a class="tp-caption Newspaper-Button rev-btn " href="index.html@p=6100" target="_self" id="slide-6100-layer-5" data-x="['center','left','left','left']" data-hoffset="['0','53','53','30']" data-y="['top','top','top','top']" data-voffset="['280','331','301','245']" data-fontsize="['12','13','13','13']" data-fontweight="['400','700','700','700']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;" data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(16, 158, 208, 0.80);"

                                       data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="on" data-responsive="off"

                                       style="z-index: 8; white-space: nowrap; font-size: 12px; font-weight: 400;font-family:Merriweather;background-color:rgba(16, 158, 208, 1.00);padding:10px 30px 10px 30px;border-color:rgba(255, 255, 255, 0);border-style:solid;border-width:2px;border-radius:2px 2px 2px 2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">READ MORE </a>
                                </li>
                            </ul>
                            <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                var htmlDivCss  = "";
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv       = document.createElement("div");
                                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                        </div>
                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                            var htmlDivCss  = ".tp-caption.Newspaper-Button,.Newspaper-Button{color:rgba(255,255,255,1.00);font-size:13px;line-height:17px;font-weight:700;font-style:normal;font-family:Roboto;padding:12px 35px 12px 35px;text-decoration:none;background-color:rgba(255,255,255,0);border-color:rgba(255,255,255,0.25);border-style:solid !important;border-width:1px !important;border-radius:0px 0px 0px 0px;text-align:left;letter-spacing:3px;border-width:2px !important}.tp-caption.Newspaper-Button:hover,.Newspaper-Button:hover{color:rgba(0,0,0,1.00);text-decoration:none;background-color:rgba(255,255,255,1.00);border-color:rgba(255,255,255,1.00);border-style:solid;border-width:1px;border-radius:0px 0px 0px 0px;cursor:pointer}.tp-caption.Newspaper-Subtitle,.Newspaper-Subtitle{color:rgba(168,216,238,1.00);font-size:15px;line-height:20px;font-weight:900;font-style:normal;font-family:Roboto;padding:0 0 0 0px;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;text-align:left;letter-spacing:3px;text-align:center}.tp-caption.Newspaper-Title,.Newspaper-Title{color:rgba(255,255,255,1.00);font-size:50px;line-height:55px;font-weight:400;font-style:italic;font-family:\"Roboto Slab\";padding:0 0 10px 0;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;text-align:left;letter-spacing:3px;font-style:normal;text-align:center}";
                            if (htmlDiv) {
                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                            } else {
                                var htmlDiv       = document.createElement("div");
                                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                            }
                        </script>
                        <script type="text/javascript">
                            /******************************************
                             -    PREPARE PLACEHOLDER FOR SLIDER    -
                             ******************************************/

                            var setREVStartSize = function () {
                                try {
                                    var e              = new Object, i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                                    e.c                = jQuery('#rev_slider_1_1');
                                    e.responsiveLevels = [1240, 1024, 778, 480];
                                    e.gridwidth        = [1240, 1024, 778, 480];
                                    e.gridheight       = [600, 500, 400, 300];

                                    e.sliderLayout = "fullwidth";
                                    if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)}), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                                        var u = (e.c.width(), jQuery(window).height());
                                        if (void 0 != e.fullScreenOffsetContainer) {
                                            var c = e.fullScreenOffsetContainer.split(",");
                                            if (c) jQuery.each(c, function (e, i) {u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u}), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                                        }
                                        f = u
                                    } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                                    e.c.closest(".rev_slider_wrapper").css({height: f})

                                } catch (d) {console.log("Failure at Presize of Slider:" + d)}
                            };

                            setREVStartSize();

                            var tpj = jQuery;

                            var revapi1;
                            tpj(document).ready(function () {
                                if (tpj("#rev_slider_1_1").revolution == undefined) {
                                    revslider_showDoubleJqueryError("#rev_slider_1_1");
                                } else {
                                    revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                        sliderType:             "standard",
                                        jsFileLocation:         "//freya.premiumcoding.com/wp-content/plugins/revslider/public/assets/js/",
                                        sliderLayout:           "fullwidth",
                                        dottedOverlay:          "none",
                                        delay:                  9000,
                                        navigation:             {
                                            keyboardNavigation:    "off",
                                            keyboard_direction:    "horizontal",
                                            mouseScrollNavigation: "off",
                                            mouseScrollReverse:    "default",
                                            onHoverStop:           "off",
                                            touch:                 {
                                                touchenabled:        "on",
                                                swipe_threshold:     75,
                                                swipe_min_touches:   50,
                                                swipe_direction:     "horizontal",
                                                drag_block_vertical: false
                                            }
                                            ,
                                            arrows:                {
                                                style:             "uranus",
                                                enable:            true,
                                                hide_onmobile:     false,
                                                hide_onleave:      true,
                                                hide_delay:        200,
                                                hide_delay_mobile: 1200,
                                                tmp:               '',
                                                left:              {
                                                    h_align:  "left",
                                                    v_align:  "center",
                                                    h_offset: 0,
                                                    v_offset: 0
                                                },
                                                right:             {
                                                    h_align:  "right",
                                                    v_align:  "center",
                                                    h_offset: 0,
                                                    v_offset: 0
                                                }
                                            }
                                            ,
                                            bullets:               {
                                                enable:            true,
                                                hide_onmobile:     true,
                                                hide_under:        600,
                                                style:             "uranus",
                                                hide_onleave:      true,
                                                hide_delay:        200,
                                                hide_delay_mobile: 1200,
                                                direction:         "horizontal",
                                                h_align:           "center",
                                                v_align:           "bottom",
                                                h_offset:          0,
                                                v_offset:          30,
                                                space:             5,
                                                tmp:               '<span class="tp-bullet-inner"></span>'
                                            }
                                        },
                                        responsiveLevels:       [1240, 1024, 778, 480],
                                        visibilityLevels:       [1240, 1024, 778, 480],
                                        gridwidth:              [1240, 1024, 778, 480],
                                        gridheight:             [600, 500, 400, 300],
                                        lazyType:               "smart",
                                        parallax:               {
                                            type:   "mouse",
                                            origo:  "slidercenter",
                                            speed:  2000,
                                            levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 47, 48, 49, 50, 51, 55],
                                            type:   "mouse",
                                        },
                                        shadow:                 0,
                                        spinner:                "off",
                                        stopLoop:               "on",
                                        stopAfterLoops:         0,
                                        stopAtSlide:            1,
                                        shuffle:                "off",
                                        autoHeight:             "off",
                                        disableProgressBar:     "on",
                                        hideThumbsOnMobile:     "on",
                                        hideSliderAtLimit:      0,
                                        hideCaptionAtLimit:     0,
                                        hideAllCaptionAtLilmit: 0,
                                        debugMode:              false,
                                        fallbacks:              {
                                            simplifyAll:            "off",
                                            nextSlideOnWindowFocus: "off",
                                            disableFocusListener:   false,
                                        }
                                    });
                                }
                            });
                            /*ready*/
                        </script>
                        <script>
                            var htmlDivCss = unescape("%23rev_slider_1_1%20.uranus.tparrows%20%7B%0A%20%20width%3A50px%3B%0A%20%20height%3A50px%3B%0A%20%20background%3Argba%28255%2C255%2C255%2C0%29%3B%0A%20%7D%0A%20%23rev_slider_1_1%20.uranus.tparrows%3Abefore%20%7B%0A%20width%3A50px%3B%0A%20height%3A50px%3B%0A%20line-height%3A50px%3B%0A%20font-size%3A40px%3B%0A%20transition%3Aall%200.3s%3B%0A-webkit-transition%3Aall%200.3s%3B%0A%20%7D%0A%20%0A%20%20%23rev_slider_1_1%20.uranus.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20%20opacity%3A0.75%3B%0A%20%20%7D%0A%23rev_slider_1_1%20.uranus%20.tp-bullet%7B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28255%2C%20255%2C%20255%2C%200%29%3B%0A%20%20-webkit-transition%3A%20box-shadow%200.3s%20ease%3B%0A%20%20transition%3A%20box-shadow%200.3s%20ease%3B%0A%20%20background%3Atransparent%3B%0A%20%20width%3A15px%3B%0A%20%20height%3A15px%3B%0A%7D%0A%23rev_slider_1_1%20.uranus%20.tp-bullet.selected%2C%0A%23rev_slider_1_1%20.uranus%20.tp-bullet%3Ahover%20%7B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28255%2C%20255%2C%20255%2C1%29%3B%0A%20%20border%3Anone%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%7D%0A%0A%23rev_slider_1_1%20.uranus%20.tp-bullet-inner%20%7B%0A%20%20-webkit-transition%3A%20background-color%200.3s%20ease%2C%20-webkit-transform%200.3s%20ease%3B%0A%20%20transition%3A%20background-color%200.3s%20ease%2C%20transform%200.3s%20ease%3B%0A%20%20top%3A%200%3B%0A%20%20left%3A%200%3B%0A%20%20width%3A%20100%25%3B%0A%20%20height%3A%20100%25%3B%0A%20%20outline%3A%20none%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background-color%3A%20rgb%28255%2C%20255%2C%20255%29%3B%0A%20%20background-color%3A%20rgba%28255%2C%20255%2C%20255%2C%200.3%29%3B%0A%20%20text-indent%3A%20-999em%3B%0A%20%20cursor%3A%20pointer%3B%0A%20%20position%3A%20absolute%3B%0A%7D%0A%0A%23rev_slider_1_1%20.uranus%20.tp-bullet.selected%20.tp-bullet-inner%2C%0A%23rev_slider_1_1%20.uranus%20.tp-bullet%3Ahover%20.tp-bullet-inner%7B%0A%20transform%3A%20scale%280.4%29%3B%0A%20-webkit-transform%3A%20scale%280.4%29%3B%0A%20background-color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%7D%0A");
                            var htmlDiv    = document.getElementById('rs-plugin-settings-inline-css');
                            if (htmlDiv) {
                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                            }
                            else {
                                var htmlDiv       = document.createElement('div');
                                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                            }
                        </script>
                    </div><!-- END REVOLUTION SLIDER -->                                </div>
            </div>


            <div class="block2">
                <div class="block2_content">
                    <div class="block2_text">
                        <p>Hello, I am <b>Freyja</b>. I am a goddess living in Valhalla. This is my blog, where I post my photos, stories and the latest updates from my world. Never miss out on new stuff. I started with Freyja to provide you with <b>daily fresh new ideas</b> about Nordic stories. It is a very clean and elegant Wordpress Theme suitable for every blogger. You can contact me at: <a href="mailto:info@freyja.com"><b>info@freyja.com</b></a> and I will gladly answer all your questions you might have about my stories and events from the Nordic world.
                            <br></p>
                    </div>
                    <div class="block2_img">
                        <img class="block2_img_big" src="{{asset('assets/img/freyja-avatar-1.jpg')}}" alt="Image">
                    </div>
                </div>
            </div>


            <div class="block1">
                <div class="block1_inner">
                    <a href="index.html#" title="Image">

                        <div class="block1_img">
                            <img src="{{asset('assets/img/freyja-small-post-1.jpg')}}" alt="PLANNING ">
                        </div>

                        <div class="block1_all_text">
                            <div class="block1_text">
                                <p>PLANNING </p>
                            </div>
                            <div class="block1_lower_text">
                                <p>WAR PLANS</p>
                            </div>
                        </div>
                    </a>
                    <a href="category/characters/index.html" title="Image">

                        <div class="block1_img">
                            <img src="{{asset('assets/img/freyja-small-post-2.jpg')}}" alt="TREE OF THE">
                        </div>

                        <div class="block1_all_text">
                            <div class="block1_text">
                                <p>TREE OF THE</p>
                            </div>
                            <div class="block1_lower_text">
                                <p>GODS</p>
                            </div>
                        </div>

                    </a>
                    <a href="category/events/index.html" title="Image">
                        <div class="block1_img">
                            <img src="{{asset('assets/img/freyja-small-post-3.jpg')}}" alt="BATTLE OF THE">
                        </div>

                        <div class="block1_all_text">
                            <div class="block1_text">
                                <p>BATTLE OF THE</p>
                            </div>
                            <div class="block1_lower_text">
                                <p>NORTH</p>
                            </div>
                        </div>

                    </a>
                </div>
            </div>
        </header>
        <div class="sidebars-wrap top">
            <div class="sidebars">
                <div class="sidebar-left-right"></div>
            </div>
        </div>


        <!-- main content start -->
        <div class="mainwrap blog home sidebar default">
            <div class="main clearfix">

                @yield('content')

            </div>

        </div>

        <div class="totop">
            <div class="gototop">
                <div class="arrowgototop"></div>
            </div>
        </div>
        <!-- footer-->
        <div class="sidebars-wrap bottom">
            <div class="sidebars">
                <div class="sidebar-fullwidth">
                    <div class="widget category_select_slider_posts"><h3>Freya Post slider</h3>
                        <div class="widget-line"></div>
                        <div class="category_select_slider_posts-widget-5" style="float:left;">

                            <ul class="bxslider-category_select_slider_posts-widget-5">

                                <li>
                                    <div class="widgett">

                                        <div class="imgholder">
                                            <a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">
                                                <img width="220" height="150" src="{{asset('assets/img/freyja-image-16-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-16" />
                                            </a>
                                        </div>
                                        <div class="wttitle"><h4><a href="index.html@p=4752" rel="bookmark" title="Permanent Link to The Story of Arthas Menethil">The Story of Arthas Menethil</a></h4></div>
                                    </div>
                                </li>


                                <li>
                                    <div class="widgett">

                                        <div class="imgholder">
                                            <a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">
                                                <img width="220" height="150" src="{{asset('assets/img/freyja-image-1-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-1" />
                                            </a>
                                        </div>
                                        <div class="wttitle"><h4><a href="index.html@p=2501" rel="bookmark" title="Permanent Link to Nordic battle">Nordic battle</a></h4></div>
                                    </div>
                                </li>


                                <li>
                                    <div class="widgett">

                                        <div class="imgholder">
                                            <a href="index.html@p=6100" rel="bookmark" title="Permanent Link to Ancient halls of Valhalla">
                                                <img width="220" height="150" src="{{asset('assets/img/freyja-image-13-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-13" />
                                            </a>
                                        </div>
                                        <div class="wttitle"><h4><a href="index.html@p=6100" rel="bookmark" title="Permanent Link to Ancient halls of Valhalla">Ancient halls of Valhalla</a></h4></div>
                                    </div>
                                </li>


                                <li>
                                    <div class="widgett">

                                        <div class="imgholder">
                                            <a href="index.html@p=6198" rel="bookmark" title="Permanent Link to Winter is here">
                                                <img width="220" height="150" src="{{asset('assets/img/freyja-image-6-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-6" />
                                            </a>
                                        </div>
                                        <div class="wttitle"><h4><a href="index.html@p=6198" rel="bookmark" title="Permanent Link to Winter is here">Winter is here</a></h4></div>
                                    </div>
                                </li>


                                <li>
                                    <div class="widgett">

                                        <div class="imgholder">
                                            <a href="index.html@p=4174" rel="bookmark" title="Permanent Link to Arthas Manethil">
                                                <img width="220" height="150" src="{{asset('assets/img/arthas_menethil-wallpaper-1280x800-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="arthas_menethil-wallpaper-1280x800" />
                                            </a>
                                        </div>
                                        <div class="wttitle"><h4><a href="index.html@p=4174" rel="bookmark" title="Permanent Link to Arthas Manethil">Arthas Manethil</a></h4></div>
                                    </div>
                                </li>


                                <li>
                                    <div class="widgett">

                                        <div class="imgholder">
                                            <a href="index.html@p=1484" rel="bookmark" title="Permanent Link to Attack of the Dragon">
                                                <img width="220" height="150" src="{{asset('assets/img/freyja-image-11-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="freyja-image-11" />
                                            </a>
                                        </div>
                                        <div class="wttitle"><h4><a href="index.html@p=1484" rel="bookmark" title="Permanent Link to Attack of the Dragon">Attack of the Dragon</a></h4></div>
                                    </div>
                                </li>


                                <li>
                                    <div class="widgett">

                                        <div class="imgholder">
                                            <a href="index.html@p=6343" rel="bookmark" title="Permanent Link to This Is Audio Post">
                                                <img width="220" height="150" src="{{asset('assets/img/amory11-220x150.jpg')}}" class="attachment-freya-widget size-freya-widget wp-post-image" alt="amory11" />
                                            </a>
                                        </div>
                                        <div class="wttitle"><h4><a href="index.html@p=6343" rel="bookmark" title="Permanent Link to This Is Audio Post">This Is Audio Post</a></h4></div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <input id="slider_id" type="hidden" value="category_select_slider_posts-widget-5">
                        <input id="slider" type="hidden" value="5">

                    </div>
                </div>
                <div class="sidebar-left-right"></div>
            </div>
        </div>
        <!-- footer-->


        <footer>

            <div class="block3">

                <a href="https://www.instagram.com/arthas_freya/" target="_blank">
                    <h4 class="block3-instagram-title">FOLLOW ME ON INSTAGRAM</h4>
                </a>
            </div>
            <div id="sb_instagram" class="sbi sbi_col_8" style="width:100%; " data-id="4175019602" data-num="8" data-res="full" data-cols="8" data-options='{&quot;sortby&quot;: &quot;none&quot;, &quot;headercolor&quot;: &quot;&quot;, &quot;imagepadding&quot;: &quot;0&quot;}'>
                <div id="sbi_images" style="padding: 0px;">
                    <div class="sbi_loader fa-spin"></div>
                </div>
                <div id="sbi_load" style="padding-top: 5px"></div>
            </div>
            <div class="footer-social">
                <div class="footer-social-inside">
                    <a target="_blank" href="http://twitter.com/PremiumCoding" title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a><a target="_blank" href="https://www.facebook.com/PremiumCoding" title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a><a target="_blank" href="https://dribbble.com/gljivec" title="Dribbble"><i class="fa fa-dribbble"></i><span>Dribbble</span></a><a target="_blank" href="https://www.flickr.com/" title="Flickr"><i class="fa fa-flickr"></i><span>Flickr</span></a><a target="_blank" href="http://www.pinterest.com/gljivec/" title="Pinterest"><i class="fa fa-pinterest"></i><span>Pinterest</span></a><a target="_blank" href="index.html#" title="Google"><i class="fa fa-google-plus"></i><span>Google</span></a><a target="_blank" href="https://www.instagram.com/dreamypixels/" title="Instagram"><i class="fa fa-instagram"></i><span>Instagram</span></a>
                </div>
            </div>

            <!-- footer bar at the bootom-->
            <div id="footerbwrap">
                <div id="footerb">
                    <div class="lowerfooter">
                        <div class="copyright">
                            © 2016 copyright PremiumCoding. All rights reserved. Freya was made with love by <a href="http://premiumcoding.com/">Premiumcoding</a></div>
                    </div>
                </div>
            </div>
            </div>
        </footer>
        <script type="text/javascript">
            function revslider_showDoubleJqueryError(sliderID) {
                var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                errorMessage     = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                jQuery(sliderID).show().html(errorMessage);
            }
        </script>
        <link rel='stylesheet' id='su-content-shortcodes-css' href='{{asset('assets/css/content-shortcodes.css')}}' type='text/css' media='all' />
        <link rel='stylesheet' id='su-box-shortcodes-css' href='{{asset('assets/css/box-shortcodes.css')}}' type='text/css' media='all' />
        <script type='text/javascript' src='{{asset('assets/js/jquery.form.min.js')}}'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var _wpcf7 = {"loaderUrl": "http:\/\/freya.premiumcoding.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif", "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}, "sending": "Sending ..."};
            /* ]]> */
        </script>
        <script type='text/javascript' src='{{asset('assets/js/scripts.js')}}'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var sb_instagram_js_options = {"sb_instagram_at": "4175019602.3a81a9f.5eba4eaaa32c4185962d81586a0a0e32"};
            /* ]]> */
        </script>
        <script type='text/javascript' src='{{asset('assets/js/sb-instagram.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.fitvids.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/retina.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.prettyPhoto.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.easing.1.3.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.cycle.all.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/gistfile_pmc.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/core.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/widget.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/tabs.min.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/wp-embed.min.js')}}'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var milapfbwidgetvars = {"app_id": "503595753002055", "select_lng": "en_US"};
            /* ]]> */
        </script>
        <script type='text/javascript' src='{{asset('assets/js/fb.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.validationEngine-en.js')}}'></script>
        <script type='text/javascript' src='{{asset('assets/js/jquery.validationEngine.js')}}'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var wysijaAJAX = {"action": "wysija_ajax", "controller": "subscribers", "ajaxurl": "http:\/\/freya.premiumcoding.com\/wp-admin\/admin-ajax.php", "loadingTrans": "Loading...", "is_rtl": ""};
            /* ]]> */
        </script>
        <script type='text/javascript' src='{{asset('assets/js/front-subscribers.js')}}'></script>
        <style type="text/css">

            .essb_links_list li.essb_totalcount_item .essb_t_l_big .essb_t_nb:after, .essb_links_list li.essb_totalcount_item .essb_t_r_big .essb_t_nb:after {
                color: #777777;
                content: "shares";
                display: block;
                font-size: 11px;
                font-weight: normal;
                text-align: center;
                text-transform: uppercase;
                margin-top: -5px;
            }

            .essb_links_list li.essb_totalcount_item .essb_t_l_big, .essb_links_list li.essb_totalcount_item .essb_t_r_big {
                text-align: center;
            }

            .essb_displayed_sidebar .essb_links_list li.essb_totalcount_item .essb_t_l_big .essb_t_nb:after, .essb_displayed_sidebar .essb_links_list li.essb_totalcount_item .essb_t_r_big .essb_t_nb:after {
                margin-top: 0px;
            }

            .essb_displayed_sidebar_right .essb_links_list li.essb_totalcount_item .essb_t_l_big .essb_t_nb:after, .essb_displayed_sidebar_right .essb_links_list li.essb_totalcount_item .essb_t_r_big .essb_t_nb:after {
                margin-top: 0px;
            }

            .essb_totalcount_item_before, .essb_totalcount_item_after {
                display: block !important;
            }

            .essb_totalcount_item_before .essb_totalcount, .essb_totalcount_item_after .essb_totalcount {
                border: 0px !important;
            }

            .essb_counter_insidebeforename {
                margin-right: 5px;
                font-weight: bold;
            }

            .essb_width_columns_1 li {
                width: 100%;
            }

            .essb_width_columns_1 li a {
                width: 92%;
            }

            .essb_width_columns_2 li {
                width: 49%;
            }

            .essb_width_columns_2 li a {
                width: 86%;
            }

            .essb_width_columns_3 li {
                width: 32%;
            }

            .essb_width_columns_3 li a {
                width: 80%;
            }

            .essb_width_columns_4 li {
                width: 24%;
            }

            .essb_width_columns_4 li a {
                width: 70%;
            }

            .essb_width_columns_5 li {
                width: 19.5%;
            }

            .essb_width_columns_5 li a {
                width: 60%;
            }

            .essb_width_columns_6 li {
                width: 16%;
            }

            .essb_width_columns_6 li a {
                width: 55%;
            }

            .essb_links li.essb_totalcount_item_before, .essb_width_columns_1 li.essb_totalcount_item_after {
                width: 100%;
                text-align: left;
            }

            .essb_network_align_center a {
                text-align: center;
            }

            .essb_network_align_right .essb_network_name {
                float: right;
            }</style>
        <style type="text/css"></style>
        <script type="text/javascript">var essb_clicked_lovethis = false;
            var essb_love_you_message_thanks                     = "Thank you for loving this.";
            var essb_love_you_message_loved                      = "You already love this today.";
            var essb_lovethis                                    = function (oInstance) {
                if (essb_clicked_lovethis) {
                    alert(essb_love_you_message_loved);
                    return;
                }
                var element = jQuery('.essb_' + oInstance);
                if (!element.length) { return; }
                var instance_post_id = jQuery(element).attr("data-essb-postid") || "";
                var cookie_set       = essb_get_lovecookie("essb_love_" + instance_post_id);
                if (cookie_set) {
                    alert(essb_love_you_message_loved);
                    return;
                }
                if (typeof(essb_settings) != "undefined") { jQuery.post(essb_settings.ajax_url, {'action': 'essb_love_action', 'post_id': instance_post_id, 'service': 'love', 'nonce': essb_settings.essb3_nonce}, function (data) { if (data) { alert(essb_love_you_message_thanks); }}, 'json'); }
                essb_tracking_only('', 'love', oInstance, true);
            };
            var essb_get_lovecookie                              = function (name) {
                var value = "; " + document.cookie;
                var parts = value.split("; " + name + "=");
                if (parts.length == 2) return parts.pop().split(";").shift();
            };
            var essb_window                                      = function (oUrl, oService, oInstance) {
                var element           = jQuery('.essb_' + oInstance);
                var instance_post_id  = jQuery(element).attr("data-essb-postid") || "";
                var instance_position = jQuery(element).attr("data-essb-position") || "";
                var wnd;
                var w                 = 800;
                var h                 = 500;
                if (oService == "twitter") {
                    w = 500;
                    h = 300;
                }
                var left = (screen.width / 2) - (w / 2);
                var top  = (screen.height / 2) - (h / 2);
                if (oService == "twitter") { wnd = window.open(oUrl, "essb_share_window", "height=300,width=500,resizable=1,scrollbars=yes,top=" + top + ",left=" + left); } else { wnd = window.open(oUrl, "essb_share_window", "height=500,width=800,resizable=1,scrollbars=yes,top=" + top + ",left=" + left); }
                if (typeof(essb_settings) != "undefined") {
                    if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } }
                    if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); }
                }
                essb_self_postcount(oService, instance_post_id);
                var pollTimer = window.setInterval(function () {
                    if (wnd.closed !== false) {
                        window.clearInterval(pollTimer);
                        essb_smart_onclose_events(oService, instance_post_id);
                    }
                }, 200);
            };
            var essb_self_postcount                              = function (oService, oCountID) {
                if (typeof(essb_settings) != "undefined") {
                    oCountID = String(oCountID);
                    jQuery.post(essb_settings.ajax_url, {'action': 'essb_self_postcount', 'post_id': oCountID, 'service': oService, 'nonce': essb_settings.essb3_nonce}, function (data) { if (data) { }}, 'json');
                }
            };
            var essb_smart_onclose_events                        = function (oService, oPostID) {
                if (typeof (essbasc_popup_show) == 'function') { essbasc_popup_show(); }
                if (typeof essb_acs_code == 'function') { essb_acs_code(oService, oPostID); }
            };
            var essb_tracking_only                               = function (oUrl, oService, oInstance, oAfterShare) {
                var element = jQuery('.essb_' + oInstance);
                if (oUrl == "") { oUrl = document.URL; }
                var instance_post_id  = jQuery(element).attr("data-essb-postid") || "";
                var instance_position = jQuery(element).attr("data-essb-position") || "";
                if (typeof(essb_settings) != "undefined") {
                    if (essb_settings.essb3_stats) { if (typeof(essb_handle_stats) != "undefined") { essb_handle_stats(oService, instance_post_id, oInstance); } }
                    if (essb_settings.essb3_ga) { essb_ga_tracking(oService, oUrl, instance_position); }
                }
                essb_self_postcount(oService, instance_post_id);
                if (oAfterShare) { essb_smart_onclose_events(oService, instance_post_id); }
            };
            var essb_pinterest_picker                            = function (oInstance) {
                essb_tracking_only('', 'pinterest', oInstance);
                var e = document.createElement('script');
                e.setAttribute('type', 'text/javascript');
                e.setAttribute('charset', 'UTF-8');
                e.setAttribute('src', '//assets.pinterest.com/js/pinmarklet.js?r=' + Math.random() * 99999999);
                document.body.appendChild(e);
            };</script>
    </body>


</html>